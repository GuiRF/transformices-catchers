# coding: utf-8

import urllib2
import re
import time
import webbrowser
import threading

url = raw_input("URL: ")

urlf = urllib2.urlopen(url)
data = urlf.read()

found = False

for num, line in enumerate(data.split("\n")):
	if re.search(".swf", line):
		line = line.split("\"")
		for n, l in enumerate(line):
			if re.search(".swf", l):
				if re.findall("(.+)=", line[int(n)-1]) and not re.search("fpdownload", l) and not found:
					l = re.findall("(.+)\?(d|n|)", l)[0] if re.findall("(.+)\?(d|n|)", l) else l

					l = l[0] if type(l) == tuple else l

					print("[FOUND] %ith line: %s" %(num, l))
					dire = "%s/%s" %(url, l)
					ask = raw_input("Do you want to download %s? [Y/N]: " %(dire))
					if ask.lower() == "y":
						F1 = urllib2.urlopen(dire)
						data = F1.read()

						f = open(l, "wb")
						f.write(data)
						f.close()

						print("[OKAY] File data was written: %s." %(l))
					found = True

if not found:
	print("[ERROR] None found.")
	ask = raw_input("Open website %s? [Y/N]: " %(url))
	if ask.lower() == "y":
		webbrowser.open(url)
		print("Website opened.")
time.sleep(2)